package com.gl.main;
import com.gl.Exception.NegitiveException;
import com.gl.service.*;
import java.io.IOException;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
public class App extends Thread{//Extending thread from Thread class
	static Scanner sc;
	public App ()//constructor
	{
		sc=new Scanner(System.in);
	}
	//run method
	public void run()
	{
		System.out.println("-------------------------------------------------------------------------------------------------------------");
		System.out.println("                         *********Wellcome to SURABHI RESTAURANT********");
		System.out.println("-------------------------------------------------------------------------------------------------------------");
		String choice="y";
		while(choice.equals("y"))
		{
			System.out.println("1.User Menu\n2.Admin Menu");
			int input=sc.nextInt();
			if(input==1)
			{
				userMenu();
			}
			if(input==2)
			{
				adminMenu();
			}
			System.out.println("Do you u want to continue Application(y/n)");
			choice=sc.next();
		}
	}
	//userMenu
	public  void userMenu()
	{
		String choice="y";
		while(choice.equals("y"))
		{
			UserServiceImpl userService= new UserServiceImpl();
			System.out.println("1.Register");
			System.out.println("2.login");
			System.out.println("3.view items");
			System.out.println("4.order items");
			System.out.println("5.TotalBill of customer");
			System.out.println("0.logout");
			int ch=sc.nextInt();
			switch(ch)
			{
			case 1:
				try {
					userService .register();
				} catch (NegitiveException e) {
					
					e.printStackTrace();
				}
				break;
			case 2:
				userService.login();
				break;
			case 3:
				userService.retreiveItem();
				break;
			case 4:
				userService.orderitems();
				break;
			case 5:
				userService.totalBillCustomer();
				break;
			case 0:
				userService.logout();
				break;
			default:
				System.out.println("Enter valid choice");
			}
			System.out.println("Do you u want to continue User Services(y/n)");
			choice=sc.next();
		}
		
	}
	//adminMenu	
	public  void adminMenu()	
	{
		String choice="y";
		while(choice.equals("y"))
		{
			AdminServiceImpl adminService=new AdminServiceImpl();
			System.out.println("1.Login");
			System.out.println("2.Insert items");
			System.out.println("3.Delete items");
			System.out.println("4.Update items");
			System.out.println("5.Display items");
			System.out.println("6.SeeAll bills");
			System.out.println("7.Today bills");
			System.out.println("8.Month bills");
			System.out.println("0.Logout");
			int ch=sc.nextInt();
			switch(ch)
			{
			case 1:
				adminService.login();
				break;
			case 2:
				adminService.insertItem();
				break;
			case 3:
				adminService.deleteItem();
				break;
			case 4:
				adminService.updateItem();
				break;
			case 5:
				adminService.retreiveItems();
				break;
			case 6:
				adminService.seebills();
				break;
			case 7:
				adminService.todayBill();
				break;
			case 8:
				adminService.monthBill();
				break;
			case 0:
				adminService.logout();
				break;
			default:
				System.out.println("Enter valid choice");
			}
			System.out.println("Do you u want to continue Admin Services(y/n)");
			choice=sc.next();
		}
		
		
	}
	//Main method
	public static void main(String args[]) throws SecurityException, IOException
	{
		//using loggers
			/*boolean append = true;
		    FileHandler handler = new FileHandler("new.log", append);

		    Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		    logger.addHandler(handler);
		     
		    logger.severe("severe message");

		    logger.warning("warning message");

		    logger.info("info message");

		    logger.config("config message");

		    logger.fine("fine message");

		    logger.finer("finer message");

		    logger.finest("finest message");*/
		    //initializing main object
			App main=new App();
			main.start();//thread  start
	}

}
