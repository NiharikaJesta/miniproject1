package com.gl.dao;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.gl.connector.DataConnector;
import com.gl.pojo.Admin;
import com.gl.pojo.Bill;
import com.gl.pojo.Item;

public class AdminDAOImpl implements AdminDAO {//this class is used to write queries to connect the database (java_sql)
	private Connection con1;
	private PreparedStatement stat;
	private Scanner sc;
	public AdminDAOImpl()
	{
		sc=new Scanner(System.in);
		con1=DataConnector.getConnect();
	}
	@Override
	public void insertItem(Item item) {
		try
		{
			stat=con1.prepareStatement("insert into items values(?,?,?)");
			stat.setInt(1, item.getItemno());
			stat.setString(2, item.getItemname());
			stat.setDouble(3, item.getPrize());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Item inserted");
			}
			else
			{
				System.out.println("Item not inserted");
			}
			}
			catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}
		
	@Override
	public void deleteItem(Item i) {
		try{
			stat=con1.prepareStatement("delete from items where itemno=?");
			stat.setInt(1, i.getItemno());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Item deletd successfully");
			}
			else
			{
				System.out.println("Item not deletd ");
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void updateItem(Item i) {
		try
		{
		stat=con1.prepareStatement("update items set prize=? ,itemname=? where itemno=?");

		stat.setDouble(1, i.getPrize());
		stat.setString(2,i.getItemname());
		stat.setInt(3, i.getItemno());
		int result=stat.executeUpdate();
		if(result>0)
		{
			System.out.println("Item updated success");
		}
		else
		{
			System.out.println("Item not updated ");
		}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public List<Item> retreiveItems() {
		List<Item> itemlist=new ArrayList<Item>();
		try {
			stat=con1.prepareStatement("select * from Items");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Item i1=new Item();
				i1.setItemno(result.getInt(1));
				i1.setItemname(result.getString(2));
				i1.setPrize(result.getDouble(3));
				itemlist.add(i1);
				
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return itemlist;
	}
	@Override
	public boolean login(Admin a) {
		try
		{
			stat=con1.prepareStatement("select * from admin where userid = ? and username=? and password = ? ");
			stat.setInt(1,a.getUserid());               
			stat.setString(2,a.getUsername());
			stat.setString(3,a.getPassword());
			ResultSet result= stat.executeQuery();
			if(result.next())
			{
				System.out.println("login successfully");
				return true;
			}
			else
			{
				System.out.println("login Failed");
				return false;
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
		
	}
	@Override
	public List<Bill> seebills() {
		List<Bill> billList=new ArrayList<Bill>();
		try {
			stat=con1.prepareStatement("select * from bill");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Bill b=new Bill();
				b.setBillno(result.getInt(1));
				b.setCustomername(result.getString(2));
				b.setItemno(result.getInt(3));
				b.setItemname(result.getString(4));
				b.setQuantity(result.getInt(5));
				b.setTprize(result.getDouble(6));
				b.setUserid(result.getInt(7));
				LocalDate d=LocalDate.now();
				b.setOrder_Date(result.getDate(8));
				billList.add(b);
		
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return billList;
	}
	@Override
	public void todayBill(Bill b) {
		try {
			stat=con1.prepareStatement(" select customername,itemname,quantity,tprize,tprize,order_Date  from bill where order_Date=?");
			stat.setDate(1,b.getOrder_Date());
			ResultSet result= stat.executeQuery();
			double total=0;
			int quantity=1;
			double prize=1;
			while(result.next()){
				
				System.out.println("customername="+result.getString(1));
				System.out.println("itemname"+result.getString(2));
				quantity=result.getInt(3);
				prize=result.getDouble(4);
				total+=prize;
				System.out.println("Quantity="+quantity);
				System.out.println("Prize="+prize);
				System.out.println("Total bill of a customer="+result.getDouble(5));
				System.out.println("Ordered Date="+result.getDate(6));
				System.out.println("----------      ");
				//total=result.getDouble(7);
			}
			System.out.println("Sum of Today bill ::"+total);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	@Override
	public void monthBill(Bill b) {
		try {
			stat=con1.prepareStatement(" select customername,itemname,quantity,tprize,tprize,order_Date  from bill where order_Date between '2022-09-27' and ?");
			stat.setDate(1,b.getOrder_Date());
			ResultSet result= stat.executeQuery();
			double total=0;
			int quantity=1;
			double prize=1;
			System.out.println("___________________________________________________________________________");
			System.out.printf("%-15s %-15s %10s %15s %15s \n","CustomerName","ItemName","Quantity","Ordered Date","  TotalPrize");
			System.out.println("___________________________________________________________________________");
			while(result.next()){
				
				System.out.printf("%-15s",result.getString(1));
				System.out.printf("%-15s",result.getString(2));
				quantity=result.getInt(3);
				prize=result.getDouble(4);
				total+=prize;
				System.out.printf("%10s",quantity);
				System.out.printf(" %15s",result.getDate(6));
				//System.out.printf("%15s",prize);
				System.out.printf("%15s",result.getDouble(5));
				System.out.printf("\n");
				//total=result.getDouble(7);
			}
			System.out.println("___________________________________________________________________________");
			System.out.println("Sum of Monthly bill                                                ::"+total);
			System.out.println("___________________________________________________________________________");
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
	}
}


//alter bill
//drop column billno  




