package com.gl.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.gl.pojo.*;
import com.gl.connector.*;
import java 
.sql.Date;
public class UserDAOImpl implements UserDAO{
	private Connection con1;
	private PreparedStatement stat;
	private Scanner sc;
	public UserDAOImpl()
	{
		sc=new Scanner(System.in);
		con1=DataConnector.getConnect();
	}
	
	@Override
	public void userRegister(User u) {
		try {
			stat=con1.prepareStatement("insert into user values(?,?,?)");
			stat.setInt(1, u.getUserid());
			stat.setString(2, u.getUsername());
			stat.setString(3, u.getPassword());
			
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Registration done successfully");
			}
			else
			{
				System.out.println("Registration not done");
			}
			}
			catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
	}


	@Override
	public List<Item> retreiveData() {
		List<Item> itemlist=new ArrayList<Item>();
		try {
			stat=con1.prepareStatement("select * from Items");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Item i1=new Item();
				i1.setItemno(result.getInt(1));
				i1.setItemname(result.getString(2));
				i1.setPrize(result.getDouble(3));
				itemlist.add(i1);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return itemlist;
	
	}

	@Override
	public void orderItems(Bill b) {
		try
		{
			List<Bill> listbill=new ArrayList<Bill>();
			stat=con1.prepareStatement("Insert into bill values(?,?,?,?,?,?,?,?)");
			stat.setInt(1,b.getBillno());
			stat.setString(2,b.getCustomername());
			stat.setInt(3,b.getItemno());
			stat.setString(4,b.getItemname());
			stat.setInt(5, b.getQuantity());
			stat.setDouble(6,b.getTprize());
			stat.setInt(7, b.getUserid());
			LocalDate d=LocalDate.now();
			//stat.setDate(8, Date.valueOf(b.getOrder_Date()));
			stat.setDate(8, b.getOrder_Date());
			int result=stat.executeUpdate();
	
			if(result>0)
			{
				System.out.println("Bill data inserted");
			}
			else
			{
				System.out.println("Bill data not inserted");
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean login(User u) {
		try {
			stat=con1.prepareStatement("select* from user where userid=? and username=?  and password=?");
			stat.setInt(1,u.getUserid());               
			stat.setString(2,u.getUsername());
			stat.setString(3,u.getPassword());
			ResultSet result= stat.executeQuery();
			if(result.next())
			{
				System.out.println("Login successfully");
				return true;
			}
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	
	}

	@Override
	public List<Bill> bill() {
		List<Bill> listbill=new ArrayList<Bill>();
		try {
			stat=con1.prepareStatement("select * from bill");
			ResultSet result= stat.executeQuery();
			while(result.next())
			{
				Bill b=new Bill();
				b.setBillno(result.getInt(1));
				b.setCustomername(result.getString(2));
				b.setItemno(result.getInt(3));
				b.setItemname(result.getString(4));
				b.setQuantity(result.getInt(5));
				b.setTprize(result.getDouble(6));
				b.setUserid(result.getInt(7));
				b.setOrder_Date(result.getDate(8));
				listbill.add(b);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listbill;
	}

	@Override
	public void totalBillCustomer(Bill b) {
		try {
			stat=con1.prepareStatement(" select customername,itemno,itemname,quantity,order_Date,tprize from bill where userid=? and order_Date=?");
			stat.setInt(1, b.getUserid());
			stat.setDate(2,b.getOrder_Date());
			ResultSet result= stat.executeQuery();
			while(result.next()){
		
				System.out.println("Customername :"+result.getString(1));
				System.out.println("Itemno       :"+result.getInt(2));
				System.out.println("itemname     :"+result.getString(3));
				System.out.println("Quantity     :"+result.getInt(4));
				System.out.println("Ordered Date :"+result.getDate(5));
				System.out.println("---------------------------");
				System.out.println("Total bill of a customer="+result.getDouble(6));
				System.out.println("---------------------------");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

		
}
