package com.gl.dao;

import java.util.List;

import com.gl.pojo.*;

public interface AdminDAO {
	//admin interface method declarations
	public boolean login(Admin a);
	public void insertItem(Item item);
	public void deleteItem(Item i);
	public void updateItem(Item i);
	public List<Item> retreiveItems();
	public List<Bill> seebills();
	public void todayBill(Bill b);
	public void monthBill(Bill b);
}
