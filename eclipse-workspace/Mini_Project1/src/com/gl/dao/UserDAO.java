package com.gl.dao;

import java.util.List;
import com.gl.pojo.*;

public interface UserDAO {
	//user interface method declarations
	public void userRegister(User u);
	public boolean login(User u);
	public List<Item> retreiveData();
	public void orderItems(Bill b);
	public List<Bill> bill();
	public void totalBillCustomer(Bill b);
}
