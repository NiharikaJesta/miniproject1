package com.gl.service;

import com.gl.Exception.NegitiveException;

public interface UserService {
	//user interface method declarations
	public void login();
	public void register()throws NegitiveException;
	public void logout();
	public void retreiveItem();
	public void orderitems();
	public void totalBillCustomer();
	
}
