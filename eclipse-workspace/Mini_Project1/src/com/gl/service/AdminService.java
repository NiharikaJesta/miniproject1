package com.gl.service;

import java.util.List;

import com.gl.pojo.Bill;

public interface AdminService {
	//Admin service interface method declarations
	public void login();
	public void logout();
	public void todayBill();
	public void monthBill();
	public void insertItem();
	public void deleteItem();
	public void updateItem();
	public void retreiveItems();
	public void seebills();
}
