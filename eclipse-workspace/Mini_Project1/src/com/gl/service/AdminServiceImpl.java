package com.gl.service;
import java.util.*;

import com.gl.dao.*;
import com.gl.pojo.Admin;
import com.gl.pojo.Bill;
import com.gl.pojo.Item;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
public class AdminServiceImpl implements AdminService  {
	private Scanner sc;
	AdminDAOImpl admindao;
	UserDAOImpl userdao;
	UserServiceImpl u;
	public AdminServiceImpl()
	{
		sc=new Scanner(System.in);
		admindao=new AdminDAOImpl();
		userdao=new UserDAOImpl();
		u=new UserServiceImpl();
	}
	//admin login
	@Override
	public void login() {
		Admin checkAdmin=new Admin();
		System.out.println("Enter Userid");
		checkAdmin.setUserid(sc.nextInt());
		System.out.println("Enter user name ");
		checkAdmin.setUsername(sc.next());
		System.out.println("Enter PAssword ");
		checkAdmin.setPassword(sc.next());
		admindao.login(checkAdmin);
		
	}
	//admin logout
	@Override
	public void logout() {
		System.out.println("*****Thank you*****");
		System.exit(0);
		
	}
	//Admin can see today bills
	@Override
	public  void todayBill() {
		System.out.println("************The total bill of Today************ ");
		LocalDate d= LocalDate.now();
		Bill b=new Bill();
		Date date1 =  Date.valueOf(LocalDate.now());
		b.setOrder_Date(date1);
		admindao.todayBill(b);
	
    }
	
	//Admin can see monthly bills
	@Override
	public void monthBill() {
		System.out.println("************The Monthly************");
		LocalDate d= LocalDate.now();
		LocalDate newmonth=d.plusMonths(1);
		Bill b=new Bill();
		Date date1 =  Date.valueOf(d.plusMonths(1));
		b.setOrder_Date(date1);
		admindao.monthBill(b);
	}
	//Admin can insert items 
	@Override
	public void insertItem() {
		System.out.println("Enter no of items u want to insert ");
		int noofitems=sc.nextInt();
		for(int x=1;x<=noofitems;x++)
		{
			Item item =new Item();
			System.out.println(".....inserting items....");
			System.out.println("Enter the itemno");
			item.setItemno(sc.nextInt());
			System.out.println("Enetr the itemname");
			item.setItemname(sc.next());
			System.out.println("Enetr the prize");
			item.setPrize(sc.nextDouble());
			System.out.println("Item inserted  successfully");
			admindao.insertItem(item);
		}
		
	}
	//Admin can delete items 
	@Override
	public void deleteItem() {
		System.out.println("Enter the itemno which u want to delete");
		int itemno=sc.nextInt();
		Item deleteItem=new Item();
		deleteItem.setItemno(itemno);
		admindao.deleteItem(deleteItem);
	}
	//Admin can update item
	@Override
	public void updateItem() {
		System.out.println("Enter Itemno which u want to update");
		int itemno=sc.nextInt();
		System.out.println("Enter new name ");
		String itemname=sc.next();
		System.out.println("Enter new prize ");
		double prize=sc.nextDouble();

		Item updateItem=new Item();
		updateItem.setItemno(itemno);
		updateItem.setItemname(itemname);
		updateItem.setPrize(prize);
		admindao.updateItem(updateItem);
		
	}
	//displaying all items
	@Override
	public void retreiveItems() {
		List<Item> list=admindao.retreiveItems();
		System.out.println("------------------------------------");
		System.out.printf("%-8s %-20s %-6s\n","ItemNo","ItemName","Prize");
		System.out.println("------------------------------------");
		for(Item item:list)
		{
			System.out.printf("%-6d",item.getItemno());
			System.out.printf("   %-20s", item.getItemname());
			System.out.printf("%6.2f" , item.getPrize());
			System.out.printf("\n");
		}
		System.out.println("------------------------------------");
	}
	//Admin can see all bills
	@Override
	public void seebills() {
		List<Bill> billist=admindao.seebills();
		System.out.println("--------------------------------------------------------------------------------------------------------");
		System.out.printf("%-10s %-15s %-10s %-15s %-10s %-15s %-10s %-15s\n" ,"Bill_No","Customer_Name","Item_no","Item_Name","Quantity","Total prize","Userid","Order_date");
		System.out.println("--------------------------------------------------------------------------------------------------------");
		for(Bill bill: billist)
		{
			System.out.printf("%-10d",bill.getBillno());
			System.out.printf(" %-15s",bill.getCustomername());
			System.out.printf(" %-10d",bill.getItemno());
			System.out.printf(" %-15s",bill.getItemname());
			System.out.printf("  %-10d",bill.getQuantity());
			System.out.printf("%10.2f",bill.getTprize());
			System.out.printf("      %-10d",bill.getUserid());
			System.out.printf(" %-15s",bill.getOrder_Date());
			System.out.printf("\n");
		}
		System.out.println("--------------------------------------------------------------------------------------------------------");
		
	}
}
