package com.gl.service;
import java.time.LocalDate;
import java.util.*;
import com.gl.Exception.NegitiveException;
import com.gl.dao.UserDAOImpl;
import com.gl.pojo.*;
import java.sql.Date;
public class UserServiceImpl implements UserService{

	private Scanner sc;
	User user;
	UserDAOImpl daoimpl;
	public UserServiceImpl()
	{
		sc=new Scanner(System.in);
		user=new User();
		daoimpl=new UserDAOImpl();
	}
	//registering the user
	@Override
	public void register()throws NegitiveException {
	try
	{
		System.out.println("Enter no of user u want to register ");
		int noofusers=sc.nextInt();
		for(int x=1;x<=noofusers;x++)
		{
			User u1=new User();
			System.out.println("-----Register------");
			System.out.println("Enter userid");
			int userid=sc.nextInt();
			u1.setUserid(userid);
			System.out.println("Enter username");
			String username=sc.next();
			u1.setUsername(username);
			System.out.println("Enter password");
			String password=sc.next();
			u1.setPassword(password);
			System.out.println("----------------------------------------------");
			System.out.println("Registration done successfully");
			System.out.println("       *Thank you*");
			daoimpl.userRegister(u1);
		}
	}
	catch(Exception e)
	{
		System.out.println(e.getMessage());
	}

	}
	//display of all items
	@Override
	public void retreiveItem() {
		
		List<Item> data=new ArrayList<Item>();
		List<Item> list=daoimpl.retreiveData();
		System.out.println("------------------------------------");
		System.out.printf("%-8s %-20s %-6s\n","ItemNo","ItemName","Prize");
		System.out.println("------------------------------------");
		for(Item item:list)
		{
			System.out.printf("%-6d",item.getItemno());
			System.out.printf("   %-20s", item.getItemname());
			System.out.printf("%6.2f" , item.getPrize());
			System.out.printf("\n");
		}
		System.out.println("------------------------------------");

	}
	//ordering by user
	@Override
	public void orderitems() {
		System.out.println("Enter no of items u want to order ");
		List<Item> list=daoimpl.retreiveData();
		Bill b=new Bill();
		retreiveItem();
		boolean bool=true;
		System.out.println("Select items from the above table");
		System.out.println("----------select your orders----------");
		System.out.println("Enter no of items u want to order");
		int noofitems=sc.nextInt();
		for(int x=1;x<=noofitems;x++) {
			System.out.println("Enter item no");
			int ino=sc.nextInt();
			b.setItemno(ino);
			for(Item i:list)
			{
				if(ino==i.getItemno()) {
					System.out.println("Enter billno");
					b.setBillno(sc.nextInt());
					System.out.println("Enter customer name");
					b.setCustomername(sc.next());
					System.out.println("Enter item name");
					b.setItemname(sc.next());
					System.out.println("Enter item Quantity u want");
					int q=sc.nextInt();
					b.setQuantity(q);
					System.out.println("Enter prize of item");
					Double p=sc.nextDouble();
					double tprize=p*q;
					b.setTprize(tprize);
					System.out.println("Enter the userid while u registerd");
					b.setUserid(sc.nextInt());
					LocalDate date=LocalDate.now();
					Date date1 =  Date.valueOf(LocalDate.now());
					b.setOrder_Date(date1);
					System.out.println("Order date is :"+date1);
					System.out.println("Your Final bill : "+tprize);
					daoimpl.orderItems(b);
					bool=true;
					break;
				}
				else
					bool=false;
			}
			if(bool=false)
				
				System.out.println("The entered item no.is not found in the item list");
		}
	}
		
	//login for user
	@Override
	public void login() {
		User user=new User();
		System.out.println("Enter User Id");
		user.setUserid(sc.nextInt());
		System.out.println("Enter User Name");
		user.setUsername(sc.next());
		System.out.println("Enter Password ");
		user.setPassword(sc.next());
		daoimpl.login(user);
	
		
		
	}
	//logout for user
	@Override
	public void logout() {
		System.out.println("*****Thank you*****");
		System.exit(0);
		
	}
	@Override
	public void totalBillCustomer() {
		System.out.println("____________Total bill of customer______________");
		System.out.println("Enter userid to see total bill");
		int userId=sc.nextInt();
		Bill b=new Bill();
		b.setUserid(userId);
		LocalDate d= LocalDate.now();
		Date date1 =  Date.valueOf(LocalDate.now());
		b.setOrder_Date(date1);
		daoimpl.totalBillCustomer(b);
	}
}
