package com.gl.testing;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.gl.dao.*;
import com.gl.pojo.*;
import com.gl.service.*;

public class TestCases {
	//declaring DAO classes
	UserDAOImpl usertest;
	AdminDAOImpl admintest;
	@Before
	public void setUp()
	{
		usertest=new  UserDAOImpl();
	}
	//test cases for user login
	@Test
	public void test() {
		User u=new User();
		u.setUserid(1);
		u.setUsername("user1");
		u.setPassword("user1");
		assertTrue(usertest.login(u));
	}
	@After
	public void showMessage()
	{
		System.out.println("user testcases passed");
	}
}
