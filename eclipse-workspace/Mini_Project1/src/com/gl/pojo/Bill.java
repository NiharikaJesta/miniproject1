package com.gl.pojo;

import java.sql.Date;
import java.time.LocalDate;

public class Bill {
	//list of attributes of bill
	private int billno;
	private String customername;
	private int itemno;
	private String itemname;
	private int quantity;
	private double tprize;
	private Date order_Date;
	// generating getters and setters
	public Date getOrder_Date() {
		return order_Date;
	}
	public void setOrder_Date(Date date) {
		this.order_Date = date;
	}
	public int getBillno() {
		return billno;
	}
	public void setBillno(int billno) {
		this.billno = billno;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public int getItemno() {
		return itemno;
	}
	public void setItemno(int itemno) {
		this.itemno = itemno;
	}
	public String getItemname() {
		return itemname;
	}
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getTprize() {
		return tprize;
	}
	public void setTprize(double tprize) {
		this.tprize = tprize;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	private int userid;

}
